#import lcd_i2c
import lcd_i2cForTest as lcd_i2c
import time


def main():
    lcd = lcd_i2c.LCDi2cMarquee(0x27, 20, 4)
    lcd.setText("Pierwsza linia", 0)
    lcd.setText("Ostatnia bardzo dluga linia", 3)
    lcd.start()
    time.sleep(6)
    lcd.lcdByte(0x04,lcd.LCDCMD)
    lcd.setText("test pierwszy", 0)
    lcd.setText("test drugi", 1)
    lcd.setText("test czwarty qwertyuiopasdf", 3)
    time.sleep(10)
    lcd.stop()
    time.sleep(10)

main()