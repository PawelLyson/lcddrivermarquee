import time
import threading
import unicodedata

class LCDi2c(object):
    LCDDATA = 1
    LCDCMD = 0
    PULSE = 0.0005
    DELAY = 0.0005
    ENABLE = 0b00000100
    BACKLIGHT = 0x08

    LINES = (0x80, 0xC0, 0x94, 0xD4)  # for lines

    def __init__(self, i2cAddr, lcdWidth, lines, i2cInterface = 1):
        self.i2cAddr = i2cAddr
        self.lcdWidth = lcdWidth
        self.lines = lines
        time.sleep(self.DELAY)

    def lcdByte(self, bits, mode):
        pass

    def lcdToggle(self, bits):
        # Toggle enable
        print("#LCD toggled#" + str(bits))

    def backlightToggle(self):
        if self.BACKLIGHT == 0x08:
            self.BACKLIGHT = 0x00
        else:
            self.BACKLIGHT = 0x08

    def writeLine(self, text, line):  # Send string to display
        message = text
        if len(message)>20:
            message += "||LONG"

        print(message)

    def getLCDWith(self):
        return self.lcdWidth

    def isBacklightON(self):
        if self.BACKLIGHT == 0x08:
            return True
        return False


class LCDi2cMarquee(LCDi2c):
    def __init__(self, i2cAddr, lcdWidth, lines, i2cInterface = 1):
        super(LCDi2cMarquee,self).__init__(i2cAddr, lcdWidth, lines, i2cInterface)
        self.sleepTime = 0.5
        self.linesText = ["", "", "", ""]
        self.isRunning = True
        self.textThread = None
        self.lock = threading.Lock()

    def setText(self, text, line, removeSpecialChars = True):
        if len(text) > 20:
            text = text + " "

        if removeSpecialChars:
            text = unicodedata.normalize('NFKD', text).encode('ascii', 'ignore')
        self.lock.acquire()
        self.linesText[line] = text
        self.lock.release()

    def marqueeText(self):
        while self.isRunning:
            print("\n\n\n\n\n\n\n\n\n\n\n\n")
            print("LCD SCREEN " + time.strftime("%H:%M:%S", time.gmtime()))
            print("------------------------")
            self.lock.acquire()
            for i in range(4):
                self.writeLine(self.linesText[i], i)
            print("------------------------")
            time.sleep(2)
            self.lock.release()

    def start(self):
        print("LCD started")
        self.isRunning = True
        self.textThread = threading._start_new_thread(self.marqueeText, ())

    def stop(self):
        time.sleep(1)
        self.lcdByte(0x08, self.LCDCMD)
        self.isRunning = False

    @staticmethod
    def moveOneChar(text):
        return text[1:] + text[0]

if __name__ == "__main__":
    print("This is module. Please import")
